const incidents = [
    {
        guid: "c2915568-7775-4757-942b-1ec791f64b22",
        trainNo: 1
    },
    {
        guid: "c2915568-7775-4757-942b-1ec791f64b23",
        trainNo: 2
    },
    {
        guid: "c2915568-7775-4757-942b-1ec791f64b24",
        trainNo: 3
    }
]

const categories = [
    {
        guid: "c2915568-7775-4757-942b-1ec791f64b22",
        name: "cat1"
    },
    {
        guid: "c2915568-7775-4757-942b-1ec791f64b23",
        name: "cat3"
    },
    {
        guid: "c2915568-7775-4757-942b-1ec791f64b24",
        name: "cat3"
    }
]

module.exports = {
    incidents,
    categories
};