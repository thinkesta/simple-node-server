const express = require('express');
const uuidv4 = require('uuid/v4');

const app = express();
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const data = require('./data');
const incidents = data.incidents;

app.get("/train/incidents", async (request, response) => {
    response.send(incidents);
});

app.post("/train/incidents/", async (request, response) => {
    const incident = {
        guid: uuidv4(),
        trainNo: request.body.trainNo,
    }
    incidents.push(incident);
    response.send(incident);
});

app.listen("3000", function() {
    console.log("Listening on port 3000...");
});
